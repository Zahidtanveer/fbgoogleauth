﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Account.aspx.vb" Inherits="Account" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h2>Welcome <%=Session("FullName").ToString()%>..!</h2>
                </div>
                <div class="panel-body">
                    <b> Login By:</b>  <%=Session("Loginby").ToString()%><br />
                    <b> Full Name:</b>  <%=Session("FullName").ToString()%><br />
                     <b>Email:</b> <%=Session("Email").ToString()%><br />
                    <b> First Name:</b> <%=Session("FirstName").ToString()%><br />
                     <b>LastName:</b> <%=Session("LastName").ToString()%><br />
                     <b>UserName:</b> <%=Session("UserName").ToString()%><br />
                     
                    <hr />
                    <asp:HyperLink ID="hlTryAgain" runat="server" Text="Back To Login" CssClass="btn btn-warning" NavigateUrl="~/Login.aspx" />
                 </div>
            </div>
        </div>
        <div class="col-md-3"></div>
         <script type="text/javascript">
             if (window.location.search.indexOf('popup=true') != -1) {
                 // set results to main window
                 window.opener.location = window.location;
                 // close pop-up window
                 window.close();
             }
         </script>
    </div>

</asp:Content>

