﻿
Imports System.Data

Partial Class Login
    Inherits System.Web.UI.Page

    Public Client_ID As String = ""
    Public Return_url As String = ""
    Public FacebookID As String = ""
    Public FirstName As String = ""
    Public LastName As String = ""
    Public Email As String = ""
    Public Email_address As String = ""
    Public Google_ID As String = ""
    Public Facebook_ClientID As String = ""
    Public Facbook_ReturnURL As String = ""
    Public Token As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then
            Client_ID = ConfigurationManager.AppSettings("google_clientId").ToString()
            Return_url = ConfigurationManager.AppSettings("google_RedirectUrl").ToString()

            Facebook_ClientID = ConfigurationManager.AppSettings("Facebook_ClientID").ToString()
            Facbook_ReturnURL = ConfigurationManager.AppSettings("Facbook_ReturnURL").ToString()
        End If

    End Sub

    Protected Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        Dim db As New DataAccessLayer()
        Dim IsLoggedIn As dbUser = db.Login(txtUserName.Text, txtPassword.Text)
        If (IsLoggedIn IsNot Nothing) Then
            Session("Loginby") = "Login Form"
            Session("FullName") = IsLoggedIn.FirstName + " " + IsLoggedIn.LastName
            Session("FirstName") = IsLoggedIn.FirstName
            Session("LastName") = IsLoggedIn.LastName
            Session("UserName") = IsLoggedIn.UserName
            Session("Email") = IsLoggedIn.Email
            Response.Redirect("~/Account.aspx")
        Else
            Labelinfo.Text = "Invalid UserName or Password..try again...!"
        End If
    End Sub
End Class
