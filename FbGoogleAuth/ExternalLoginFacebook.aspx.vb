﻿
Partial Class ExternalLoginFacebook
    Inherits System.Web.UI.Page
    Public AccessToken As String = ""


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim fb As FacebookAuth = New FacebookAuth()
        If Request.QueryString("access_token") IsNot Nothing Then
            AccessToken = Request.QueryString("access_token").ToString()
            Dim fbuser As FacebookUser = fb.UserInfo(AccessToken)
            Dim db As New DataAccessLayer()
            Dim IsEmailAlreadyRegistered As Boolean = db.CheckEmailExsit(fbuser.email)
            If (IsEmailAlreadyRegistered = False) Then
                db.Register(fbuser.first_name, fbuser.last_name, fbuser.email, fbuser.first_name + fbuser.last_name, "Password@123")
            End If
            Session("Loginby") = "Faccebook"
            Session("FullName") = fbuser.first_name + " " + fbuser.last_name
            Session("FirstName") = fbuser.first_name
            Session("LastName") = fbuser.last_name
            Session("UserName") = fbuser.first_name + fbuser.last_name
            Session("Email") = fbuser.email
            Response.Redirect("~/Account.aspx?popup=true")
        End If
    End Sub
End Class
