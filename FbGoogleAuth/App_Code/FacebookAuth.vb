﻿Imports System.IO
Imports System.Net
Imports Microsoft.VisualBasic
Imports Newtonsoft.Json

Public Class FacebookAuth


    Public Function UserInfo(Token As String) As FacebookUser
        If Token IsNot Nothing Then
            Dim URI As String = "https://graph.facebook.com/me?fields=email,id,first_name,last_name&access_token=" & Token
            Dim webClient As WebClient = New WebClient()
            Dim stream As Stream = webClient.OpenRead(URI)
            Dim b As String

            Using br As StreamReader = New StreamReader(stream)
                b = br.ReadToEnd()
                Dim user As FacebookUser = JsonConvert.DeserializeObject(Of FacebookUser)(b)
                Return user
            End Using
        End If
        Return Nothing
    End Function
End Class
