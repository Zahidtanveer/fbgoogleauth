﻿Imports Microsoft.VisualBasic

Public Class GoogleUser
    Public Property id As String
    Public Property email As String
    Public Property verified_email As Boolean
    Public Property name As String
    Public Property given_name As String
    Public Property family_name As String
    Public Property link As String
    Public Property picture As String
    Public Property gender As String
    Public Property locale As String
End Class
