﻿Imports System.Data
Imports System.Data.SqlClient

Public Class DataAccessLayer

    Dim strConnection As String = ConfigurationManager.ConnectionStrings("dbconnection").ConnectionString
    Dim conn As New SqlConnection(strConnection)

    Public Sub DataAccessLayer()
    End Sub

    'Check if Email already Exsit
    Public Function CheckEmailExsit(ByVal Email As String) As Boolean
        Dim com As New SqlCommand("spCheckIsEmailExsit", conn)
        com.CommandType = CommandType.StoredProcedure
        Dim p1 As New SqlParameter("email", Email)
        com.Parameters.Add(p1)
        If conn.State <> ConnectionState.Open Then
            conn.Open()
        End If
        Dim rd As SqlDataReader = com.ExecuteReader()
        If rd.HasRows Then
            rd.Read()
            conn.Close()
            Return True
        Else
            Return False
        End If
    End Function

    'Check if UserName already Exsit
    Public Function CheckUserNameExsit(ByVal UserName As String) As Boolean
        Dim com As New SqlCommand("spCheckIsUserNameExsit", conn)
        com.CommandType = CommandType.StoredProcedure
        Dim p1 As New SqlParameter("username", UserName)
        com.Parameters.Add(p1)
        If conn.State <> ConnectionState.Open Then
            conn.Open()
        End If
        Dim rd As SqlDataReader = com.ExecuteReader()
        If rd.HasRows Then
            rd.Read()
            conn.Close()
            Return True
        Else
            Return False
        End If
    End Function

    ' Register User
    Public Function Register(ByVal FirstName As String, ByVal LastName As String, ByVal Email As String, ByVal UserName As String, ByVal Password As String) As Boolean
        Dim cmd As New SqlCommand("spAddUsers", conn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim p1 As New SqlParameter("firstname", FirstName)
        Dim p2 As New SqlParameter("lastname", LastName)
        Dim p3 As New SqlParameter("username", UserName)
        Dim p4 As New SqlParameter("email", Email)
        Dim p5 As New SqlParameter("password", Password)

        cmd.Parameters.Add(p1)
        cmd.Parameters.Add(p2)
        cmd.Parameters.Add(p3)
        cmd.Parameters.Add(p4)
        cmd.Parameters.Add(p5)
        If conn.State <> ConnectionState.Open Then
            conn.Open()
        End If

        cmd.ExecuteNonQuery()
        conn.Close()
        Return True
    End Function

    'Login User
    Public Function Login(ByVal Email As String, ByVal Password As String) As dbUser
        Dim com As New SqlCommand("spUserLogins", conn)
        com.CommandType = CommandType.StoredProcedure
        Dim p1 As New SqlParameter("email", Email)
        Dim p2 As New SqlParameter("password", Password)
        com.Parameters.Add(p1)
        com.Parameters.Add(p2)

        If conn.State <> ConnectionState.Open Then
            conn.Open()
        End If

        Dim rd As SqlDataReader = com.ExecuteReader()
        If rd.HasRows Then
            Dim dataTable As DataTable = rd.GetSchemaTable()
            rd.Read()
            Dim user As dbUser = New dbUser()
            user.FirstName = rd("FirstName").ToString()
            user.LastName = rd("LastName").ToString()
            user.Email = rd("Email").ToString()
            user.UserName = rd("Username").ToString()
            conn.Close()
            Return user

        Else
            Return Nothing
        End If
    End Function

End Class
