﻿Imports System.IO
Imports System.Net
Imports Microsoft.VisualBasic
Imports Newtonsoft.Json

Public Class GoogleAuth


    Public Function UserInfo(Token As String) As GoogleUser

        If Token IsNot Nothing Then
            Dim URI As String = "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" & Token
            Dim webClient As WebClient = New WebClient()
            Dim stream As Stream = webClient.OpenRead(URI)
            Dim b As String

            Using br As StreamReader = New StreamReader(stream)
                b = br.ReadToEnd()
                Dim user As GoogleUser = JsonConvert.DeserializeObject(Of GoogleUser)(b)
                Return user
            End Using
        End If
        Return Nothing

    End Function

End Class
