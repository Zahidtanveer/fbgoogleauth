﻿
Partial Class ExternalLoginGoogle
    Inherits System.Web.UI.Page
    Public AccessToken As String = ""


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim google As GoogleAuth = New GoogleAuth()
        Dim fb As FacebookAuth = New FacebookAuth()
        If Request.QueryString("access_token") IsNot Nothing Then
            AccessToken = Request.QueryString("access_token").ToString()
            Dim googleUser As GoogleUser = google.UserInfo(AccessToken)
            Dim db As New DataAccessLayer()
            Dim IsEmailAlreadyRegistered As Boolean = db.CheckEmailExsit(googleUser.email)
            If (IsEmailAlreadyRegistered = False) Then
                db.Register(googleUser.given_name, googleUser.family_name, googleUser.email, googleUser.given_name + googleUser.family_name, "Password@123")
            End If
            Session("Loginby") = "Google"
            Session("FullName") = googleUser.name
            Session("FirstName") = googleUser.given_name
            Session("LastName") = googleUser.family_name
            Session("UserName") = googleUser.given_name + googleUser.family_name
            Session("Email") = googleUser.email
            Response.Redirect("~/Account.aspx?popup=true")
        End If
    End Sub
End Class
