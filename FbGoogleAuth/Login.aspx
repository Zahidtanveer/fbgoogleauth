﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <form id="form2">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h2>Login</h2>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">

                            <asp:Label ID="Label1" runat="server" CssClass="control-label" Text="Username"></asp:Label>
                            <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="text-danger" ControlToValidate="txtUserName" ErrorMessage="UserName is Required..!"></asp:RequiredFieldValidator>

                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label2" runat="server" CssClass="control-label" Text="Password"></asp:Label>
                            <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtPassword" CssClass="text-danger" runat="server" ErrorMessage="Password is required...!"></asp:RequiredFieldValidator>
                        </div>

                        <div class="form-group">
                            <asp:Button ID="btnLogin" CssClass="btn btn-info" runat="server" Text="Login" />
                            <a class="btn btn-success" href="Register.aspx">Register</a>
                            <asp:Label ID="Labelinfo" runat="server" Text=""></asp:Label>
                        </div>
                        <div>
                            <a href="#" class="btn btn-primary" id="fb" onclick="javascript:OpenFaceBookLoginPopup();" ><span>Login with FaceBook</span></a>
                            <a href="#" class="btn btn-danger" id="google" onclick="OpenGoogleLoginPopup();" ><span>Login with google</span></a>
                            <script type="text/javascript">
                                function Popup(url, width, height) {
                                    if (width == undefined || !width) { width = 550; }
                                    if (height == undefined || !height) { height = 450; }
                                    var x = (screen.width - width) / 2;
                                    var y = (screen.height - height) / 2;
                                    url += '&popup=true';
                                    window.open(url, '_blank', 'toolbar=no,status=no,resizable=yes,scrollbars=yes,width=' + width + ',height=' + height + ',left=' + x + ',top=' + y);
                                    return false;
                                }

                                function OpenFaceBookLoginPopup() {

                                    var url = "https://www.facebook.com/dialog/oauth?";
                                    url += "client_id=<%=Facebook_ClientID %>";
                                    url += "&redirect_uri=<%=Facbook_ReturnURL%>/ExternalLoginFacebook.aspx&scope=email,public_profile&response_type=token";
            Popup(url);
        }
        function OpenGoogleLoginPopup() {
            var url = "https://accounts.google.com/o/oauth2/auth?";
            url += "scope=https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email&";
            url += "state=%2Fprofile&"
            url += "redirect_uri=<%=Return_url %>&"
            url += "response_type=token&"
            url += "client_id=<%=Client_ID %>";
            Popup(url);
        }

                            </script>



                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>


    </form>


</asp:Content>

