﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Register.aspx.vb" Inherits="Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form2">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h2>Register</h2>
                    </div>
                    <div class="panel-body">
                            <asp:Label ID="Labelinfo" Font-Bold="true" Font-Size="Medium"  runat="server" Text=""></asp:Label>

                        <div class="form-group">

                            <asp:Label ID="Label4" runat="server" CssClass="control-label" Text="FirstName"></asp:Label>

                            <asp:TextBox ID="txtFirstName" CssClass="form-control" runat="server"></asp:TextBox>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtFirstName" runat="server" ForeColor="Red" ErrorMessage="FirstName is Required..!"></asp:RequiredFieldValidator>

                        </div>
                        <div class="form-group">

                            <asp:Label ID="Label5" runat="server" CssClass="control-label" Text="LastName"></asp:Label>

                            <asp:TextBox ID="txtLastName" CssClass="form-control" runat="server"></asp:TextBox>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtLastName" ForeColor="Red" ErrorMessage="Last Name is Required..!"></asp:RequiredFieldValidator>

                        </div>
                        <div class="form-group">

                            <asp:Label ID="Label1" runat="server" CssClass="control-label" Text="UserName"></asp:Label>

                            <asp:TextBox ID="txtUserName" CssClass="form-control" runat="server"></asp:TextBox>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtUserName" runat="server" ForeColor="Red" ErrorMessage="UserName is Required..!"></asp:RequiredFieldValidator>

                        </div>
                        <div class="form-group">

                            <asp:Label ID="Label2" runat="server" CssClass="control-label" Text="Email"></asp:Label>

                            <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server"></asp:TextBox>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail" ForeColor="Red" ErrorMessage="Email is Required..!"></asp:RequiredFieldValidator>

                        </div>
                        <div class="form-group">

                            <asp:Label ID="Label3" runat="server" CssClass="control-label" Text="Password"></asp:Label>

                            <asp:TextBox ID="txtPassword" CssClass="form-control" runat="server" TextMode="Password"></asp:TextBox>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtPassword" runat="server" ForeColor="Red" ErrorMessage="Password is Required..!"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            
                             <asp:Button ID="btnRegister" runat="server"
                                OnClick="btnRegister_Click" CssClass="btn btn-info" Text="Register"  />
                            <a class="btn btn-success" href="Login.aspx">Login If Already Registerd</a><br />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
        <div>
         


        </div>
    </form>
</asp:Content>

