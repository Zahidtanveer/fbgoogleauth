create table Users(Id int primary key identity(1,1),
FirstName varchar(200),
LastName varchar(200),
Username varchar(200),
Email varchar(200),
Password varchar(30)
)

create PROCEDURE spAddUsers(
@firstname varchar(200),
@lastname varchar(200),
@username varchar(200),
@email varchar(200),
@password varchar(30)
)
as
insert into Users  values(@firstname,@lastname,@username,@email,@password)

 create PROCEDURE spUserLogins(
@email as varchar(200),
@password as varchar(30)
)
AS
SELECT * FROM Users WHERE Email=@email AND Password=@password

create PROCEDURE spCheckIsEmailExsit(@email as varchar(200))
AS
SELECT * FROM Users WHERE Email= @email

create PROCEDURE spCheckIsUserNameExsit(@username as varchar(200))
AS
SELECT * FROM Users WHERE UserName= @username